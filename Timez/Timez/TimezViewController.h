//
//  TimezViewController.h
//  Timez
//
//  Created by Malin Pihl on 2012-03-17.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TimezViewController : UIViewController {
    
}
- (void)goToPage: (id)sender;
- (void)configureToolBar;

@end
