//
//  DayData.h
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WorkRecord.h"

@interface DayData : NSObject


-(void) addWorkData:(WorkRecord *) wr;
-(void) removeWorkData:(WorkRecord *) wr;
-(void) updateWorkData:(WorkRecord *) wr;

@property (assign) BOOL *isCurrent;  //YES this is default customer
@property (assign) NSDate *dayDate;
@property (assign) NSMutableArray *workDataLogged;
@property (assign) NSTimeInterval *lunchTime;  //not sure if this should be used.

@end
