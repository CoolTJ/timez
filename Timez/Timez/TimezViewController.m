//
//  TimezViewController.m
//  Timez
//
//  Created by Malin Pihl on 2012-03-17.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TimezViewController.h"
#import "TimezAppDelegate.h"
#import "StartViewController.h"

@implementation TimezViewController


- (void)goToPage: (id)sender
{
    NSLog(@"Switch view soon");
}

- (void)configureToolBar
{
    UIBarButtonItem *startButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tabbar_sell.png"] style:UIBarButtonItemStylePlain target:self action: @selector(goToPage:)] autorelease];
    [startButton setTag:1];
	[startButton setTitle:@"Start"];
	
	UIBarButtonItem *timeButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"tabbar_sell.png"] style:UIBarButtonItemStylePlain target:self action: @selector(goToPage:)] autorelease];
    [startButton setTag:2];
	[timeButton setTitle:@"Time Sheet"];
	
	UIBarButtonItem *setButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings_icon.png"] style:UIBarButtonItemStylePlain target:self action: @selector(goToPage:)] autorelease];
    [startButton setTag:3];
	[setButton setTitle:@"Settings"];
    
    // Flexible space in between
	UIBarButtonItem *flexSpace = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    
	// Set our toolbar items
	[self setToolbarItems:[NSMutableArray arrayWithObjects:startButton, flexSpace, timeButton, flexSpace, setButton, nil] animated:YES];

}


@end
