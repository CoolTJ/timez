//
//  StartViewController.h
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimezViewController.h"

@interface StartViewController : TimezViewController <UIPickerViewDelegate,UIPickerViewDataSource> {
    int picker_selection;
}

- (IBAction)startLogging:(id)sender;
@property (retain, nonatomic) IBOutlet UIButton *knappen;
@property (retain, nonatomic) IBOutlet UIPickerView *customerPicker;

-(void)prettyPlease;
-(void)login:(int)id;
-(void)logout;

@end
