//
//  WorkRecord.m
//  Timez
//
//  Created by Tobias Åkerstedt on 3/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WorkRecord.h"

////
// One work record is start-end time for one customer one day.
// several workrecords from same customer might be recorded for one day.
//
////


@implementation WorkRecord

@synthesize lunch;
@synthesize lunchIncluded;
@synthesize dayDate;
@synthesize startTime;
@synthesize endTime;

@end
