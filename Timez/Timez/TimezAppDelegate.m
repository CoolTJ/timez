//
//  TimezAppDelegate.m
//  Timez
//
//  Created by Malin Pihl on 2012-03-17.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//  Modified by Jan Pettersson added Regions for Location 2012-03-17
//

#import "TimezAppDelegate.h"
#import "RegionAnnotation.h"
#import "RegionsViewController.h"

@implementation TimezAppDelegate

@synthesize window=_window;
@synthesize timezView;
@synthesize region;
//@synthesize zeta;
@synthesize navController;


- (void) configureNavController
{
    [navController setNavigationBarHidden:YES animated:YES];
    [navController.navigationBar setBarStyle: UIBarStyleBlack];
    [navController setToolbarHidden:NO animated:YES];		

    // Configure navigation toolbar
    [navController.toolbar setBarStyle: UIBarStyleBlack];
    
    /*UIBarButtonItem *backButton = [[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_icon.png"] style:UIBarButtonItemStylePlain target:self action: @selector(goBack:)] autorelease];
	[backButton setTitle:@"Back"];*/
	
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSError *connect_error = nil;
    
    // Override point for customization after application launch.
    startView = [[StartViewController alloc] initWithNibName:@"StartViewController" bundle:[NSBundle mainBundle]];
    
    navController = [[UINavigationController alloc] initWithRootViewController:startView];
    [self configureNavController];
    [self.window addSubview:navController.view];
    
    
    //
    // Fire up a connection towards zeta
    //    
    NSString *zeta_login = @"HELO 556 team_awesomeness 1121\n\n";
    
    zeta = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];    
    [zeta connectToHost:@"jozz.no-ip.org" onPort:4711 error:&connect_error];
    
    sleep(3);   // Since it's asynch, let's wait a while
    
    if ([zeta isConnected] == TRUE) {
        NSData *result = [[NSData alloc] init];
        NSLog(@"ZETA CONNECTION");
        [zeta writeData:(NSData*)[zeta_login dataUsingEncoding:NSUTF8StringEncoding] withTimeout:-1 tag:1];
        //[zeta readDataToData:result withTimeout:5 tag:2];
        [zeta readDataToLength:100 withTimeout:-1 tag:33];
    } else {
        NSLog(@"NOT CONNECTED :(");
        NSLog([connect_error localizedDescription]);
    }

    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
    // Reset the icon badge number to zero.
	[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	
	if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
		// Stop normal location updates and start significant location change updates for battery efficiency.
		[region.locationManager stopUpdatingLocation];
		[region.locationManager startMonitoringSignificantLocationChanges];
	}
	else {
		NSLog(@"Significant location change monitoring is not available.");
	}
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
	if ([CLLocationManager significantLocationChangeMonitoringAvailable]) {
		// Stop significant location updates and start normal location updates again since the app is in the forefront.
		[region.locationManager stopMonitoringSignificantLocationChanges];
		[region.locationManager startUpdatingLocation];
	}
	else {
		NSLog(@"Significant location change monitoring is not available.");
	}
	
	if (!region.updatesTableView.hidden) {
		// Reload the updates table view to reflect update events that were recorded in the background.
		[region.updatesTableView reloadData];
        
		// Reset the icon badge number to zero.
		[UIApplication sharedApplication].applicationIconBadgeNumber = 0;
	}

    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)dealloc
{
    [navController release];
    [timezView release];
    [_window release];
    [super dealloc];
}

#pragma mark Socket callbacks

- (void)socket:(GCDAsyncSocket *)sender didReadData:(NSData *)data withTag:(long)tag
{
    NSLog(@"Reading %d, got: %s", tag, [data bytes]); 
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    if (tag == 1)
        NSLog(@"First request sent");
    else if (tag == 2)
        NSLog(@"Second request sent");
}

@end
